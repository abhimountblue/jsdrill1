

function problem5(inventory) {
  const carsArray = [];
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index]["car_year"] < 2000) {
      carsArray.push(inventory[index]);
    }
  }
  console.log(carsArray.length);
  return carsArray;
}


module.exports = problem5;
