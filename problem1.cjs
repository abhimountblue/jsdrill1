
function problem1(arr=[],searchId) {
    if(!Array.isArray(arr)||arr.length==0||typeof searchId!=='number'){
        return []
    }
   for(let index=0;index<arr.length;index++){
       if(arr[index]['id']==searchId){
            return [arr[index]]
       }
   }
   return []
}


module.exports=problem1;