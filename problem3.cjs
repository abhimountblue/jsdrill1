

function problem3(inventory) {
  const sortAlphabetically = [...inventory];
  for (let index = 0; index < sortAlphabetically.length - 1; index++) {
    let minIndex = index;
    for (let nextValue = index + 1; nextValue < sortAlphabetically.length; nextValue++) {
      if (
        sortAlphabetically[nextValue]["car_model"][0] <
        sortAlphabetically[minIndex]["car_model"][0]
      )
        minIndex = nextValue;
    }
    let temp = sortAlphabetically[minIndex];
    sortAlphabetically[minIndex] = sortAlphabetically[index];
    sortAlphabetically[index] = temp;
  }
  console.log(sortAlphabetically);
  return;
}

module.exports = problem3;
